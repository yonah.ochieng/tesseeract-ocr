﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows;
using Tesseract;

namespace a9t9Ocr
{
    class TesseractOcr : ITesseractOrc
    {
        public string Language { get; set; }
        private readonly string _pathToTestData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\(a9t9)OcrDesktop\tessdata";
        public string docType = "ID_KE_FRONT";
        public TesseractOcr(string lang)
        {
            Language = lang;
        }

        public void ChangeLanguage(string lang)
        {
            Language = lang;
        }
        public string RecognizeOneImage(ImageClass image)
        {
            if (image == null)
                return string.Empty;
            return BeginRecognize(image.FilePath);
        }

        public List<string> RecognizeFewImages(List<ImageClass> images)
        {
            return images.Select(imageClass => BeginRecognize(imageClass.FilePath)).ToList();
        }
        
        private string BeginRecognize(string pathToImage)
        {
            try
            {
                if (!File.Exists(pathToImage))
                    return "Image not found";
                DoConfigs doConfigs = new DoConfigs();
                DoConfig config = doConfigs.doConfigs.Find(x => x.docType == docType);
                var resultText = "";
                if (config != null)
                {
                    using (var engine = new TesseractEngine(_pathToTestData, Language, EngineMode.Default))
                    {
                        //using (var img = Pix.LoadFromFile(pathToImage))
                        Image image = Image.FromFile(pathToImage);
                        if(config.WHRation > 0)
                        {
                            image = ResizeImage(image, config.WHRation);
                        }
                        int imgWidth = image.Width;
                        int imgHeight = image.Height;

                        foreach (DocAttribute docAttribute in config.docAttributes)
                        {
                            int x = (int)(imgWidth * docAttribute.xFactor);
                            int y = (int)(imgHeight * docAttribute.yFactor);

                            int w = (int)(imgWidth * docAttribute.wFactor);
                            int h = (int)(imgHeight * docAttribute.hFactor);


                            var cropped = CropImage(image, x, y, w, h);

                            using (var img = Pix.LoadFromMemory(cropped))
                            {
                                using (var page = engine.Process(img))
                                {
                                    var resultTextCropped = page.GetText();
                                    if (!String.IsNullOrEmpty(resultTextCropped))
                                        resultText = String.Format("{0} \r\n {1}", resultText, resultTextCropped);
                                        //return resultText;
                                }
                            }

                        }
                    }
                }

                return resultText;


            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                MessageBox.Show(e.StackTrace);
                return null;
            }
            return null;
        }


        public static byte[] CropImage(Image source, int x, int y, int width, int height)
        {
            Rectangle crop = new Rectangle(x, y, width, height);

            var bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }

            byte[] data = default(byte[]);

            using (System.IO.MemoryStream sampleStream = new System.IO.MemoryStream())

            {

                //save to stream.

                bmp.Save(sampleStream, System.Drawing.Imaging.ImageFormat.Bmp);

                //the byte array

                data = sampleStream.ToArray();

            }

            return data;
        }
        public static Image ResizeImage(Image orgImg, double WHRatio)
        {
            int sourceWidth = orgImg.Width;
            int sourceHeight = orgImg.Height;

            int destHeight = (int)(sourceWidth / WHRatio);

            Bitmap b = new Bitmap(sourceWidth, destHeight);

            Graphics g = Graphics.FromImage((Image)b);

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(orgImg, 0, 0, sourceWidth, destHeight);

            g.Dispose();

            return (Image)b;
        }
    }
    

    class DoConfigs
    {
        public List<DoConfig> doConfigs = new  List<DoConfig>();
        public DoConfigs()
        {
            List<DocAttribute> docAttribute1 = new List<DocAttribute>();
            docAttribute1.Add(new DocAttribute() { 
                attributeType = "TEXT",
                attributeName = "FULL_NAME",
                xFactor = 0.24783836164141100,
                yFactor = 0.31693989071038200,
                wFactor = 0.51749810516463500,
                hFactor = 0.06976320582877960

            });

            docAttribute1.Add(new DocAttribute()
            {
                attributeType = "TEXT",
                attributeName = "CERT_NUMBER",
                xFactor = 0.11870503475136500,
                yFactor = 0.60965391621129300,
                wFactor = 0.23616990611451100,
                hFactor = 0.06320582877959930

            });


            //ID Front Kenya
            List<DocAttribute> docAttributeIDKeFront = new List<DocAttribute>();
            docAttributeIDKeFront.Add(new DocAttribute()
            {
                attributeType = "TEXT",
                attributeName = "SERIAL_NUMBER",
                xFactor = 0.21611668173066000,
                yFactor = 0.14258647631046100,
                wFactor = 0.22814937890864400,
                hFactor = 0.10805122713960000

            });
            docAttributeIDKeFront.Add(new DocAttribute()
            {
                attributeType = "TEXT",
                attributeName = "ID_NUMBER",
                xFactor = 0.68885224692225800,
                yFactor = 0.13927994644623800,
                wFactor = 0.26537934936555500,
                hFactor = 0.12131236304002700

            });

            doConfigs = new List<DoConfig>() {
                new DoConfig() {
                     docType="CISA",
                     WHRation= 1.32732240437158,
                     docAttributes = docAttribute1,
                },

                new DoConfig() {
                     docType="ID_KE_FRONT",
                     WHRation= 1.4639816859841900000000,
                     docAttributes = docAttributeIDKeFront,
                }
            };

        }

    }
    class DoConfig
    {
        public string docType { set; get; } 
        public double WHRation { set; get; }
        public List<DocAttribute> docAttributes { set; get; }
       
    }

    class DocAttribute
    {
       public string attributeType { get; set; }
        public string attributeName { get; set; }
       public double xFactor { get; set; }
       public double yFactor { get; set; }

       public double wFactor { get; set; }
       public double hFactor { get; set; }
    }
}
